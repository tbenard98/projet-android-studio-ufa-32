# Rick & Morty Application


## Presentation

Ce projet respecte une architecture MVC avec des singletons dans un application android codé en Java.  
Cette application propose plusieurs éléments pour en apprendre plus sur l'environnement de RickEtMorty.
Trois listes y sont disponibles, une liste des personnages, une liste des planetes, ainsi qu'une liste des episodes de Rick et Morty.



## Prérequis

- Installation d’Android Studio.
- Récupérer la branche master


## Consignes respéctées

### Les basiques
- 3 Écrans avec des listes d’éléments distincts.
- Écran avec le détail d’un élément pour un personnage.
- Appel WebService à une API Rest, GET.
- Stockage de données des trois listes en cache.

### Les plus
  - Utilisation d’une architecture MVC.
  - Utilisation de Thread.
  - Utilisation de Singletons.
  - Respect des principes SOLID
  - Respect du Gitflow.
  - Effort sur le design et respect d'une certaine charte graphique pour l'ensemble de l'application.
  - Utilisation de différents Layouts pour afficher les listes.
  - Ecran de chargement avec Gif animé.


## Fonctionnalités

### Écran de chargement

- Le premier écran de chargement a l'ouverture de l'application. (Gif animé avec implémentation de Thread)

![chargement](Projet_Android/img_readme/chargement.png | width=240)

### Écran de selection

- L'écran de sélection permet de choisir la liste que l'on souhaite afficher.

![selection](Projet_Android/img_readme/selection.png | width=240)


### Liste des personnages

-	Cet écran affiche la liste des personnages de Rick & Morty.

![liste_personnages](Projet_Android/img_readme/liste_personnages.png | width=240)


### Détail d'un personnages

-	Cet ecran affiche le détail d'un personnage de Rick & Morty.

![detail_personnage](Projet_Android/img_readme/detail_personnage.png | width=240)

### Liste des planetes

-	Cet écran affiche la liste des planetes de Rick & Morty. (Malheureusement pas d'images sur l'API et trop peu de données pour que le détail d'une planète soit pertinent)

![liste_planetes](Projet_Android/img_readme/liste_planetes.png | width=240)

### Liste des épisodes

-	Cet écran affiche la liste des épisodes de Rick & Morty.(Malheureusement pas d'images sur l'API et trop peu de données pour que le détail d'un épisode soit pertinent)

![liste_episodes](Projet_Android/img_readme/liste_episodes.png | width=240)

