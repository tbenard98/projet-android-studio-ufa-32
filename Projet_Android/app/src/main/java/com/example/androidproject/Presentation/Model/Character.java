package com.example.androidproject.Presentation.Model;

public class Character {

    private String url;
    private String name;
    private String image;
    private String status;
    private String species;
    private String type;
    private String gender;

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return image;
    }

    public String getGender() {
        return gender;
    }

    public String getSpecies() {
        return species;
    }

    public String getType() {
        return type;
    }

    public String getStatus() {
        return status;
    }
}
