package com.example.androidproject.Data;

import com.example.androidproject.Presentation.Model.RestLocationResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RickAndMortyLocationApi {
    void onSuccess(RestLocationResponse response);
    void onFailed();

    @GET("/api/location")
    Call<RestLocationResponse> getRestLocationResponse();
}
