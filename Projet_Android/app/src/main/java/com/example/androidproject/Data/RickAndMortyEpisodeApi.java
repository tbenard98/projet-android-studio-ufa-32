package com.example.androidproject.Data;

import com.example.androidproject.Presentation.Model.RestEpisodeResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RickAndMortyEpisodeApi {
    void onSuccess(RestEpisodeResponse response);
    void onFailed();

    @GET("/api/episode")
    Call<RestEpisodeResponse> getRestEpisodeResponse();
}
