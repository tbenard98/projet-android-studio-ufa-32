package com.example.androidproject.Presentation.Model;

import java.util.List;

public class RestLocationResponse {

    private Integer count;
    private String next;
    private List<Location> results;

    public Integer getCount() {
        return count;
    }

    public List<Location> getResults() {
        return results;
    }

    public String getNext() {
        return next;
    }
}
