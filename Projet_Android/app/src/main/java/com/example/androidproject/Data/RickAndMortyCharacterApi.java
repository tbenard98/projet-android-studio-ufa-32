package com.example.androidproject.Data;

import com.example.androidproject.Presentation.Model.RestCharacterResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RickAndMortyCharacterApi {
    void onSuccess(RestCharacterResponse response);
    void onFailed();

    @GET("/api/character")
    Call<RestCharacterResponse> getRestCharacterResponse();
}
