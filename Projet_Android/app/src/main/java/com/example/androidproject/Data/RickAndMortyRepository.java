package com.example.androidproject.Data;

import android.content.SharedPreferences;

import com.example.androidproject.Presentation.Model.Character;
import com.example.androidproject.Presentation.Model.RestCharacterResponse;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RickAndMortyRepository {

    private Gson gson;
    private RickAndMortyCharacterApi rickAndMortyCharacterApi;
    private SharedPreferences sharedPreferences;

    public RickAndMortyRepository(RickAndMortyCharacterApi rickAndMortyCharacterApi, SharedPreferences sharedPreferences) {
        this.rickAndMortyCharacterApi = rickAndMortyCharacterApi;
        this.sharedPreferences = sharedPreferences;
    }

    public void getCharacterResponse(RickAndMortyCharacterApi callback){
        rickAndMortyCharacterApi.getRestCharacterResponse().enqueue(new Callback<RestCharacterResponse>() {
        public void onResponse(Call<RestCharacterResponse> call, Response<RestCharacterResponse> response) {
            if(response.isSuccessful() && response.body() != null){
                callback.onSuccess(response.body());
                List<Character> characterList = response.body().getResults();
            }
            else{
                callback.onFailed();
            }
        }

        @Override
        public void onFailure(Call<RestCharacterResponse> call, Throwable t) {
            callback.onFailed();
        }
    });
    }
}
