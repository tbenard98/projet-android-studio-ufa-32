package com.example.androidproject.Presentation.View;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.androidproject.Presentation.Model.Location;
import com.example.androidproject.R;

import java.util.List;

public class LocationListAdapter extends RecyclerView.Adapter<LocationListAdapter.ViewHolder> {
    private List<Location> values;

    class ViewHolder extends RecyclerView.ViewHolder {
        View layout;
        TextView txtHeader;
        ImageView icon;

        ViewHolder(View v) {
            super(v);
            layout = v;
             txtHeader = (TextView) v.findViewById(R.id.episode_name);
             icon = (ImageView) v.findViewById(R.id.episode_image);
        }
    }

    public void add(int position, Location item) {
        values.add(position, item);
        notifyItemInserted(position);
    }

    private void remove(int position) {
        values.remove(position);
        notifyItemRemoved(position);
    }

    public LocationListAdapter(List<Location> myDataset) {
        values = myDataset;
    }

    @Override
    public LocationListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v =
                inflater.inflate(R.layout.location_layout, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Location currentLocation = values.get(position);
        holder.txtHeader.setText(currentLocation.getName());
        holder.icon.setImageResource(R.drawable.planet);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return values.size();
    }

}
