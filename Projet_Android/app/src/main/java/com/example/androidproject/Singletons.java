package com.example.androidproject;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.androidproject.Data.RickAndMortyCharacterApi;
import com.example.androidproject.Data.RickAndMortyEpisodeApi;
import com.example.androidproject.Data.RickAndMortyLocationApi;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Singletons {

    private static Gson gsonInstance;
    private static RickAndMortyCharacterApi rickAndMortyCharacterApiInstance;
    private static RickAndMortyLocationApi rickAndMortyLocationApiInstance;
    private static RickAndMortyEpisodeApi rickAndMortyEpisodeApiInstance;
    private static SharedPreferences sharedPreferences;


    public static Gson getGson(){
        if(gsonInstance == null){
        return new GsonBuilder()
                .setLenient()
                .create();
        }
        return gsonInstance;

    }

    public static RickAndMortyCharacterApi getRichAndMortyCharacterApi(){
        if(rickAndMortyCharacterApiInstance == null){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(getGson()))
                    .build();

            rickAndMortyCharacterApiInstance = retrofit.create(RickAndMortyCharacterApi.class);
        }
        return rickAndMortyCharacterApiInstance;
    }

    public static RickAndMortyLocationApi getRichAndMortyLocationApi(){
        if(rickAndMortyLocationApiInstance == null){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(getGson()))
                    .build();

            rickAndMortyLocationApiInstance = retrofit.create(RickAndMortyLocationApi.class);
        }
        return rickAndMortyLocationApiInstance;
    }

    public static RickAndMortyEpisodeApi getRichAndMortyEpisodeApi(){
        if(rickAndMortyEpisodeApiInstance == null){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(getGson()))
                    .build();

            rickAndMortyEpisodeApiInstance = retrofit.create(RickAndMortyEpisodeApi.class);
        }
        return rickAndMortyEpisodeApiInstance;
    }

    public static SharedPreferences getSharedPreferences(Context context){
        if(sharedPreferences == null){
            sharedPreferences = context.getSharedPreferences("application_esiea", Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }
}
