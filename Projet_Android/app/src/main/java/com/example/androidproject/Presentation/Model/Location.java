package com.example.androidproject.Presentation.Model;

public class Location {
    private String url;
    private String id;
    private String name;
    private String type;
    private String dimension;

    public String getUrl() {
        return url;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getDimension() {
        return dimension;
    }
}
