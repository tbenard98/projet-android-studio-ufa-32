package com.example.androidproject.Presentation.Model;

public class Episode {
    private String id;
    private String name;
    private String air_date;
    private String episode;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAir_date() {
        return air_date;
    }

    public String getEpisode() {
        return episode;
    }

}
