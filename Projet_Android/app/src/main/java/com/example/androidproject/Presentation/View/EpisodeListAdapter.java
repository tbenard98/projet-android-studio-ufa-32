package com.example.androidproject.Presentation.View;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidproject.Presentation.Model.Episode;
import com.example.androidproject.R;

import java.util.List;

public class EpisodeListAdapter extends RecyclerView.Adapter<EpisodeListAdapter.ViewHolder> {
    private List<Episode> values;

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtHeader;
        TextView txtFooter;
        ImageView icon;
        View layout;

        ViewHolder(View v) {
            super(v);
            layout = v;
            txtFooter = (TextView) v.findViewById(R.id.episode_number);
            txtHeader = (TextView) v.findViewById(R.id.episode_name);
            icon = (ImageView) v.findViewById(R.id.episode_image);
        }
    }

    public void add(int position, Episode item) {
        values.add(position, item);
        notifyItemInserted(position);
    }

    private void remove(int position) {
        values.remove(position);
        notifyItemRemoved(position);
    }

    public EpisodeListAdapter(List<Episode> myDataset) {
        values = myDataset;
    }

    @Override
    public EpisodeListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v =
                inflater.inflate(R.layout.episode_layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Episode currentEpisode = values.get(position);
        holder.txtHeader.setText(currentEpisode.getName());
        holder.txtFooter.setText(currentEpisode.getEpisode());
        holder.icon.setImageResource(R.drawable.television);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return values.size();
    }

}
