package com.example.androidproject.Presentation.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.androidproject.Presentation.Controller.MainController;
import com.example.androidproject.Presentation.Model.Character;
import com.example.androidproject.R;
import com.example.androidproject.Singletons;

import java.util.ArrayList;
import java.util.List;

public class CharacterListActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private CharacterListAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private MainController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.character_list);

        controller= new MainController(
                this,
                null,
                null,
                Singletons.getGson(),
                Singletons.getSharedPreferences(getApplicationContext())

        );
        controller.onStart();
    }

    public void showList(List<Character> pokemonList){
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        List<String> input = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            input.add("Test" + i);
        }
        mAdapter = new CharacterListAdapter(pokemonList, new CharacterListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Character item) {
                controller.onItemClick(item);
            }
        });
        recyclerView.setAdapter(mAdapter);
    }

    public void showError(){
        Toast.makeText(getApplicationContext(),"API Error", Toast.LENGTH_SHORT).show();
    }

    public void navigateToDetails(Character character){
        Intent myIntent = new Intent(this, CharacterActivity.class);
        myIntent.putExtra("imageURL", character.getImageUrl());
        myIntent.putExtra("name", character.getName());
        myIntent.putExtra("status", character.getStatus());
        myIntent.putExtra("species", character.getSpecies());
        myIntent.putExtra("type", character.getType());
        myIntent.putExtra("gender", character.getGender());
        CharacterListActivity.this.startActivity(myIntent);
    }
}
