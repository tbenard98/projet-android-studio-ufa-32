package com.example.androidproject.Presentation.Controller;

import android.content.SharedPreferences;
import android.widget.Toast;

import com.example.androidproject.Presentation.Model.Episode;
import com.example.androidproject.Presentation.Model.Location;
import com.example.androidproject.Presentation.Model.RestEpisodeResponse;
import com.example.androidproject.Presentation.Model.RestLocationResponse;
import com.example.androidproject.Presentation.View.EpisodeListActivity;
import com.example.androidproject.Presentation.View.LocationListActivity;
import com.example.androidproject.Singletons;
import com.example.androidproject.Presentation.Model.Character;
import com.example.androidproject.Presentation.Model.RestCharacterResponse;
import com.example.androidproject.Presentation.View.CharacterListActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainController {
    private SharedPreferences sharedPreferences;
    private Gson gson;
    private CharacterListActivity characterListActivity;
    private LocationListActivity locationListActivity;
    private EpisodeListActivity episodeListActivity;

    public MainController(CharacterListActivity mainActivity, LocationListActivity locationListActivity, EpisodeListActivity episodeListActivity, Gson gson, SharedPreferences sharedPreferences){
        this.characterListActivity = mainActivity;
        this.locationListActivity = locationListActivity;
        this.episodeListActivity = episodeListActivity;
        this.gson = gson;
        this.sharedPreferences = sharedPreferences;
    }

    public void onStart(){

        if(characterListActivity != null){
            List<Character> characterList = getCharacterDataFromCache();

            if(characterList !=   null) {
                characterListActivity.showList(characterList);
            }
            else{
                makeCharacterApiCall();
            }
        }
        else if(locationListActivity != null){
            List<Location> locationList = getLocationDataFromCache();
            if(locationList !=   null) {
                locationListActivity.showList(locationList);
            }
            else {
                makeLocationApiCall();
            }

        }
        else if(episodeListActivity != null){
            List<Episode> episodeList = getEpisodeDataFromCache();
            if(episodeList !=   null) {
                episodeListActivity.showList(episodeList);
            }
            else {
                makeEpisodeApiCall();
            }
        }
    }

    public void onItemClick(Character character){
        characterListActivity.navigateToDetails(character);
    }

    private void makeCharacterApiCall(){
        Call<RestCharacterResponse> call = Singletons.getRichAndMortyCharacterApi().getRestCharacterResponse();
        call.enqueue(new Callback<RestCharacterResponse>() {
            @Override
            public void onResponse(Call<RestCharacterResponse> call, Response<RestCharacterResponse> response) {
                if(response.isSuccessful() && response.body() != null){
                    List<Character> characterList = response.body().getResults();
                    saveCharacterList(characterList);
                    characterListActivity.showList(characterList);
                }
                else{
                    characterListActivity.showError();
                }
            }

            @Override
            public void onFailure(Call<RestCharacterResponse> call, Throwable t) {
                characterListActivity.showError();
            }
        });

    }

    private void makeLocationApiCall() {
        Call<RestLocationResponse> call = Singletons.getRichAndMortyLocationApi().getRestLocationResponse();
        call.enqueue(new Callback<RestLocationResponse>() {
            @Override
            public void onResponse(Call<RestLocationResponse> call, Response<RestLocationResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    List<Location> locationList = response.body().getResults();
                    saveLocationList(locationList);
                    locationListActivity.showList(locationList);
                } else {
                    characterListActivity.showError();
                }
            }

            @Override
            public void onFailure(Call<RestLocationResponse> call, Throwable t) {
                characterListActivity.showError();
            }
        });
    }

    private void makeEpisodeApiCall(){
        Call<RestEpisodeResponse> call = Singletons.getRichAndMortyEpisodeApi().getRestEpisodeResponse();
        call.enqueue(new Callback<RestEpisodeResponse>() {
            @Override
            public void onResponse(Call<RestEpisodeResponse> call, Response<RestEpisodeResponse> response) {
                if(response.isSuccessful() && response.body() != null){
                    List<Episode> episodeList = response.body().getResults();
                    saveEpisodeList(episodeList);
                    episodeListActivity.showList(episodeList);
                }
                else{
                    characterListActivity.showError();
                }
            }

            @Override
            public void onFailure(Call<RestEpisodeResponse> call, Throwable t) {
                characterListActivity.showError();
            }
        });

    }

    private List<Character> getCharacterDataFromCache() {
        String jsonPokemon = sharedPreferences.getString("jsonCharacterList",null);
        if(jsonPokemon == null) {
            return null;
        } else{
            Type listType = new TypeToken<List<Character>>(){}.getType();
            return gson.fromJson(jsonPokemon,listType);
        }
    }

    private List<Episode> getEpisodeDataFromCache() {
        String jsonPokemon = sharedPreferences.getString("jsonEpisodeList",null);
        if(jsonPokemon == null) {
            return null;
        } else{
            Type listType = new TypeToken<List<Episode>>(){}.getType();
            return gson.fromJson(jsonPokemon,listType);
        }
    }

    private List<Location> getLocationDataFromCache() {
        String jsonPokemon = sharedPreferences.getString("jsonLocationList",null);
        if(jsonPokemon == null) {
            return null;
        } else{
            Type listType = new TypeToken<List<Location>>(){}.getType();
            return gson.fromJson(jsonPokemon,listType);
        }
    }

    private void saveCharacterList(List<Character> characterList) {
        String jsonString =  gson.toJson(characterList);
        sharedPreferences
                .edit()
                .putString("jsonCharacterList",jsonString)
                .apply();

        Toast.makeText(characterListActivity.getApplicationContext(),"List saved", Toast.LENGTH_SHORT).show();
    }

    private void saveLocationList(List<Location> locationList) {
        String jsonString =  gson.toJson(locationList);
        sharedPreferences
                .edit()
                .putString("jsonLocationList",jsonString)
                .apply();

        Toast.makeText(locationListActivity.getApplicationContext(),"List saved", Toast.LENGTH_SHORT).show();
    }

    private void saveEpisodeList(List<Episode> episodeList) {
        String jsonString =  gson.toJson(episodeList);
        sharedPreferences
                .edit()
                .putString("jsonEpisodeList",jsonString)
                .apply();

        Toast.makeText(episodeListActivity.getApplicationContext(),"List saved", Toast.LENGTH_SHORT).show();
    }
}
