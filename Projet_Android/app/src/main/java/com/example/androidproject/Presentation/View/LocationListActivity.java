package com.example.androidproject.Presentation.View;

import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidproject.Presentation.Controller.MainController;
import com.example.androidproject.Presentation.Model.Location;
import com.example.androidproject.R;
import com.example.androidproject.Singletons;

import java.util.ArrayList;
import java.util.List;

public class LocationListActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private LocationListAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private MainController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_list);

        controller= new MainController(
                null,
                this,
                null,
                Singletons.getGson(),
                Singletons.getSharedPreferences(getApplicationContext())

        );
        controller.onStart();
    }

    public void showList(List<Location> locationList){
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        List<String> input = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            input.add("Test" + i);
        }
        mAdapter = new LocationListAdapter(locationList);
        recyclerView.setAdapter(mAdapter);
    }

    public void showError(){
        Toast.makeText(getApplicationContext(),"API Error", Toast.LENGTH_SHORT).show();
    }

//    public void navigateToDetails(Character character){
//        Intent myIntent = new Intent(this, CharacterActivity.class);
//        myIntent.putExtra("imageURL", character.getImageUrl());
//        myIntent.putExtra("name", character.getName());
//        myIntent.putExtra("status", character.getStatus());
//        myIntent.putExtra("species", character.getSpecies());
//        myIntent.putExtra("type", character.getType());
//        myIntent.putExtra("gender", character.getGender());
//        .this.startActivity(myIntent);
//    }
}
