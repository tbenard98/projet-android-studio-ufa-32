package com.example.androidproject.Presentation.View;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.androidproject.R;
import com.example.androidproject.Singletons;

public class SplashScreenActivity extends AppCompatActivity {

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_splash_screen);
            displayGif();


            Thread background = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(4000);
                        Singletons.getRichAndMortyCharacterApi();
                        Singletons.getRichAndMortyEpisodeApi();
                        Singletons.getRichAndMortyLocationApi();
                        Intent intent = new Intent(SplashScreenActivity.this, SelectActivity.class);
                        SplashScreenActivity.this.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            background.start();
        }

        private void displayGif(){
            ImageView pickleView = findViewById(R.id.pickleView);
            Glide.with(this).load(R.drawable.pickle).into(pickleView);

            ImageView mortyView = findViewById(R.id.mortyView);
            Glide.with(this).load(R.drawable.giphy).into(mortyView);
        }
    }
