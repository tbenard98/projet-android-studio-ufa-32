package com.example.androidproject.Presentation.View;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.androidproject.R;
import com.squareup.picasso.Picasso;

import java.io.IOException;

public class CharacterActivity extends AppCompatActivity {
    private ImageView imageView;
    private TextView nameView;
    private TextView statusView;
    private TextView speciesView;
    private TextView typeView;
    private TextView genderView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.character_activity);
        try {
            this.showCharacter();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showCharacter() throws IOException {
        Intent intent = getIntent();
        String imageUrl = intent.getStringExtra("imageURL");
        String name = intent.getStringExtra("name");
        String status = intent.getStringExtra("status");
        String species = intent.getStringExtra("species");
        String type = intent.getStringExtra("type");
        String gender = intent.getStringExtra("gender");

        nameView = findViewById(R.id.textView1);
        statusView = findViewById(R.id.textView2);
        speciesView = findViewById(R.id.textView3);
        typeView = findViewById(R.id.textView4);
        genderView = findViewById(R.id.textView5);

        imageView = findViewById(R.id.imageView);
        Picasso.with(this.getApplicationContext()).load(imageUrl).into(imageView);
        imageView.setAlpha(230);

        if(type.equals("")){
            type = "Unknow";
        }

        this.nameView.setText(name);
        this.statusView.setText(status);
        this.speciesView.setText(species);
        this.typeView.setText(type);
        this.genderView.setText(gender);
    }
}
