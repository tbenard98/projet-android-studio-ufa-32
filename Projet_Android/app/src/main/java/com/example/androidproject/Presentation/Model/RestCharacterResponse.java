package com.example.androidproject.Presentation.Model;

import java.util.List;

public class RestCharacterResponse {

    private Integer count;
    private String next;
    private List<Character> results;

    public Integer getCount() {
        return count;
    }

    public List<Character> getResults() {
        return results;
    }

    public String getNext() {
        return next;
    }
}
