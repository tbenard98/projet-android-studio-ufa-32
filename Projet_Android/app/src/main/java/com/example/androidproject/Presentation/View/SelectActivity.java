package com.example.androidproject.Presentation.View;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.androidproject.Presentation.Model.Location;
import com.example.androidproject.R;

public class SelectActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_activity);
        Button characterButton = this.findViewById(R.id.characterButton);
        Button locationButton = this.findViewById(R.id.locationButton);
        Button episodeButton = this.findViewById(R.id.episodeButton);
        characterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), CharacterListActivity.class);
                v.getContext().startActivity(intent);
            }
        });

        locationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), LocationListActivity.class);
                v.getContext().startActivity(intent);
            }
        });

        episodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), EpisodeListActivity.class);
                v.getContext().startActivity(intent);
            }
        });
    }
}
