package com.example.androidproject.Presentation.Model;

import java.util.List;

public class RestEpisodeResponse {

    private Integer count;
    private String next;
    private List<Episode> results;

    public Integer getCount() {
        return count;
    }

    public List<Episode> getResults() {
        return results;
    }

    public String getNext() {
        return next;
    }
}
